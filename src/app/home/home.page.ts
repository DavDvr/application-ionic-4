import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public contact={
    Name:"Devaure David",
    Email:"daviddevaure@gmail.com",
    Tel:"0640460057",
    Logo:"assets/images/Logo.jpeg",
    Location:"assets/images/M2I.png"
  };
  constructor() {}
  ngOnInit() {
  }
}
