import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import { Drivers} from '@ionic/storage';
import {Storage} from '@ionic/storage';

import { IonicStorageModule } from '@ionic/storage-angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Camera} from '@ionic-native/camera/ngx';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';




@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),
    AppRoutingModule,HttpClientModule,
    IonicStorageModule.forRoot({
    name: 'MyLocations',
    driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage,Drivers.SecureStorage]
  })],
  providers: [Storage,Geolocation,Camera,SplashScreen,StatusBar,{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy,}],
  bootstrap: [AppComponent],
})
export class AppModule {}

