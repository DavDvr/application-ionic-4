import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  public menus=[
    {title:"Home", url:"/menu/home", icon:'home-outline'},
    {title:"Meteo", url:"/menu/meteo", icon:'cloud-circle-outline'},
    {title:"Gallery", url:"/menu/gallery", icon:'images-outline'},
    {title:"Locations", url:"/menu/locations", icon:'navigate-circle-outline'},
    {title:"Logout", url:"logout", icon:'log-out-outline'},
    {title:"Exit", url:"exit", icon:'power'},
  ]
  constructor(private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
  }

  onMenuAction(m) {
    if(m.url == 'logout'){
      this.authService.logout();
      this.router.navigateByUrl('/login');
    }else if(m.url == 'exit'){
      navigator['app'].exitApp();
    }else{
      this.router.navigateByUrl(m.url);
    }
  }
}
