import { Component, OnInit } from '@angular/core';
import {LocationsService} from '../services/locations.service';
import {Place} from '../model/place.model';
import {Router} from '@angular/router';
import {AlertController, NavController} from '@ionic/angular';



@Component({
  selector: 'app-locations',
  templateUrl: './locations.page.html',
  styleUrls: ['./locations.page.scss'],
})
export class LocationsPage implements OnInit {

  public locations:Array<Place>;

  constructor(private locService: LocationsService, private route:Router, public alertCtrl: AlertController, private navCtrl: NavController) { }

  ngOnInit() {

  }


  private onGetLocations() {
    this.locService.getLocation().then(data=>{
      this.locations = data;
    })
  }

  ionViewWillEnter(){
    this.onGetLocations();
  }

  onNewLocation() {
      this.route.navigateByUrl("/menu/new-location");
  }

 async onRemoveLoc(p:Place) {
   const  alert= await this.alertCtrl.create({
      cssClass:'designAlert',
      header:'Remove this Location?',
      message:'<strong>Do you want really remove it?</strong>',
        buttons:[{
          text: 'Cancel',
          role: 'Cancel',
          cssClass: 'text-dark',
          handler: () => {

          }
        },{
          text:'Remove',
          handler:()=>{
            this.removeLocation(p);
          }
        }
        ]
   });
   await alert.present();
  }

  /**
   * remove just one location
   * @param p
   */
  public removeLocation(p:Place){
    let index = this.locations.indexOf(p);
    this.locations.splice(index,1);
    this.locService.updateLocations(this.locations);

  }

  onLoadDetailsLocation(p: Place) {
    this.locService.currentLocation=p;
    this.route.navigateByUrl("menu/location-details");
  }
}
