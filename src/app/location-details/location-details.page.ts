import { Component, OnInit } from '@angular/core';
import {LocationsService} from '../services/locations.service';
import {Place} from '../model/place.model';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.page.html',
  styleUrls: ['./location-details.page.scss'],
})
export class LocationDetailsPage implements OnInit {
  private currentPlace: Place;

  constructor( private locService: LocationsService, private camera: Camera, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.currentPlace = this.locService.currentLocation;
  }


  public async onTakePicture() {
    const options: CameraOptions={
      quality:100,
      destinationType:this.camera.DestinationType.DATA_URL,
      encodingType:this.camera.EncodingType.JPEG,
      mediaType:this.camera.MediaType.PICTURE,
      sourceType:this.camera.PictureSourceType.CAMERA,
      allowEdit:true,
    };
    const options1: CameraOptions={
      quality:100,
      destinationType:this.camera.DestinationType.DATA_URL,
      encodingType:this.camera.EncodingType.JPEG,
      mediaType:this.camera.MediaType.PICTURE,
      sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit:true
    }

    let alert = await this.alertCtrl.create({
      cssClass:'customAlert',
      header:"Make a choice",
      message:"You must selected either option",
      buttons:[
        {
          text:"Camera",
          handler:()=> {
            this.getPicture(options);
          }
        },
        {
          text:"Library",
          handler:()=>{
            this.getPicture(options1);
          }
        }
      ]
    })
    await alert.present();

  }


  private getPicture(params: CameraOptions) {
    this.camera.getPicture(params)
      .then(dataImage=>{
        let base64Image = 'data:image/jpeg;base64,'+ dataImage;
        //this.currentPlace.photos.push(base64Image);
        this.locService.addPhoto(base64Image, this.currentPlace.timestamp);
    }),(err)=>{
        console.log(err);
    }
  }
}
