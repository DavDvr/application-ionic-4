import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MeteoService} from '../services/meteo.service';


@Component({
  selector: 'app-meteo',
  templateUrl: './meteo.page.html',
  styleUrls: ['./meteo.page.scss'],
})
export class MeteoPage implements OnInit {

  public city: string;
  private dataWeather;

  constructor(private serviceMeteo : MeteoService) { }

  ngOnInit() {
  }

  onLoadWeather() {
    this.serviceMeteo.getMeteoData(this.city)
      .subscribe(data=>{
          this.dataWeather=data;
      },err => {
          console.log(err);
      });
  }
}
