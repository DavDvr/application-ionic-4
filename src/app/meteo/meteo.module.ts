import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeteoPageRoutingModule } from './meteo-routing.module';

import { MeteoPage } from './meteo.page';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes=[{path:'',component:MeteoPage}]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeteoPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MeteoPage]
})
export class MeteoPageModule {}
