import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {

  constructor(private httpClient: HttpClient) { }

  public getMeteoData(city:string){
    return this.httpClient.get("https://api.openweathermap.org/data/2.5/weather?q="+city+"&APPID=07318b8d2261c41689b0b60a08795076");
  }
}
