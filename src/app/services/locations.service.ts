import { Injectable } from '@angular/core';
import {Place} from '../model/place.model';
import {Storage} from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class LocationsService {

  private locations: Array<Place> =[];
  public currentLocation: Place;

  constructor(private storage: Storage) {
    this.locations.push({title:"A"});
    this.locations.push({title:"B"});
    this.locations.push({title:"C"});
    this.init();
  }

  async init(){
    this.storage= await this.storage.create();
  }

  /**
   * return a list of places
   */
  public getLocation(){
    return this.storage.get("locations").then(data=>{
      this.locations = data != null ? data : [];
      return this.locations.slice();
    });
  }

  /**
   * add just one location
   * @param place
   */
  public addLocation(place:Place){
    this.locations.push(place);
    this.storage?.set("locations",this.locations);
  }

  /**
   * update with after remove location
   * @private
   */
  public updateLocations(locations) {
    this.storage?.set("locations",locations);
  }


  addPhoto(image: string, timestamp: number) {
    for(let i=0; i < this.locations.length;i++){
      if(this.locations[i].timestamp === timestamp){
        this.locations[i].photos.push(image);
        this.updateLocations(this.locations);
        break;
      }
    }
  }
}
