import { Component } from '@angular/core';
import {Platform} from '@ionic/angular';

import {Router} from '@angular/router';
import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private router: Router, private authService: AuthenticationService){
    this.initializeApp();
  }


  initializeApp(){
    this.platform.ready().then(()=>{
      this.login();

    });
  }


  private login() {
    let authenticated = this.authService.loadToken();
    if(authenticated){
      this.router.navigateByUrl('/menu/home');
    }else{
      this.router.navigateByUrl('/login');
    }
  }
}
